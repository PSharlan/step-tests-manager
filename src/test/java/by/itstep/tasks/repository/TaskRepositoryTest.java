package by.itstep.tasks.repository;

import by.itstep.tasks.TasksApplicationTests;
import by.itstep.tasks.entity.TaskEntity;
import by.itstep.tasks.entity.TaskTopicEntity;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class TaskRepositoryTest extends TasksApplicationTests {

    @Test
    void getById() throws Exception {
        // GIVEN
        var topicId = createTopic("Variables");
        var savedTaskId = createTask("Some task", topicId);

        // WHEN
        var foundTask = taskRepository.getById(savedTaskId).get();

        // THEN
        assertNotNull(foundTask.getId());
        assertNotNull(foundTask.getCreatedAt());
        assertNull(foundTask.getDeletedAt());
        assertNotNull(foundTask.getTopic());
        assertNotNull(foundTask.getTopic().getId());
        assertEquals("Some task", foundTask.getExercise());
    }

    @Test
    void findAll() throws Exception {
        // GIVEN
        UUID topicId = createTopic("Variables");
        createTask("Task1", topicId);
        createTask("Task2", topicId);
        createTask("Task3", topicId);
        var idToDelete = createTask("Task4", topicId);
        createTask("Task5", topicId);

        taskRepository.delete(idToDelete);

        // WHEN
        var foundTasks = taskRepository.findAll(0, 3);

        // THEN
        assertEquals(3, foundTasks.size());
        foundTasks.forEach(task -> {
            assertNotEquals("Task1", task.getExercise());
            assertNotEquals("Task4", task.getExercise());
            assertNotNull(task.getTopic());
            assertNotNull(task.getTopic().getId());
        });
    }

    @Test
    void findByTopic() throws Exception {
        // GIVEN
        var firstTopicId = createTopic("Variables");
        var secondTopicId = createTopic("Collections");
        createTask("Task1", firstTopicId);
        createTask("Task2", firstTopicId);
        createTask("Task3", firstTopicId);
        createTask("Task4", secondTopicId);
        UUID idToDelete = createTask("Task5", secondTopicId);

        taskRepository.delete(idToDelete);

        // WHEN
        var firstTopic = taskTopicRepository.getById(firstTopicId).get();
        var secondTopic = taskTopicRepository.getById(secondTopicId).get();
        var tasksWithFirstTopic = taskRepository.findByTopic(firstTopic, 0, 5);
        var tasksWithSecondTopic = taskRepository.findByTopic(secondTopic, 0, 5);

        // THEN
        assertEquals(3, tasksWithFirstTopic.size());
        assertEquals(1, tasksWithSecondTopic.size());
        tasksWithFirstTopic.forEach(task -> assertNotNull(task.getTopic().getId()));
    }

    @Test
    void save() throws Exception {
        // GIVEN
        var topicId = createTopic("Variables");
        var topic = taskTopicRepository.getById(topicId).get();

        var taskToSave = TaskEntity.builder().exercise("Exercise")
                .exerciseDescription("Some description")
                .rightAnswer("Right answer")
                .topic(topic)
                .build();

        // WHEN
        var savedId = taskRepository.save(taskToSave);
        var savedTask = taskRepository.getById(savedId).get();

        // THEN
        assertNotNull(savedTask.getId());
        assertNotNull(savedTask.getCreatedAt());
        assertNotNull(savedTask.getTopic());
        assertEquals("Variables", savedTask.getTopic().getName());
        assertNotNull(savedTask.getTopic().getId());
    }

    @Test
    void delete() throws Exception {
        // GIVEN
        var firstTopicId = createTopic("Variables");
        var secondTopicId = createTopic("Collections");
        createTask("Task1", firstTopicId);
        createTask("Task2", firstTopicId);
        createTask("Task3", firstTopicId);
        createTask("Task4", secondTopicId);
        UUID idToDelete = createTask("Task5", secondTopicId);

        // WHEN
        taskRepository.delete(idToDelete);
        var allTasks = taskRepository.findAll(0, 10);
        var deletedTask = taskRepository.getById(idToDelete);

        // THEN
        assertEquals(4, allTasks.size());
        assertFalse(deletedTask.isPresent());
    }

}