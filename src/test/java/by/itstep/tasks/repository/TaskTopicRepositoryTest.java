package by.itstep.tasks.repository;

import by.itstep.tasks.TasksApplicationTests;
import by.itstep.tasks.entity.TaskTopicEntity;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static java.time.LocalDateTime.now;
import static org.junit.jupiter.api.Assertions.*;

class TaskTopicRepositoryTest extends TasksApplicationTests {

    @Test
    void getById_happyPath() throws Exception {
        // GIVEN
        var topicName = "Variables";
        var id = createTopic(topicName);

        // WHEN
        var foundTopic = taskTopicRepository.getById(id).get();

        // THEN
        assertEquals(id, foundTopic.getId());
        assertEquals(topicName, foundTopic.getName());
    }

    @Test
    void findAll_happyPath() throws Exception {
        // GIVEN
        createTopic("Variables");
        createTopic("Collections");
        createTopic("OOP");
        createTopic("Generics");

        // WHEN
        var foundTopics = taskTopicRepository.findAll(0, 3);

        // THEN
        assertEquals(3, foundTopics.size());
        foundTopics.forEach(topic ->
                assertNotEquals("Variables", topic.getName()));
    }

    @Test
    void save_happyPath() throws Exception {
        // GIVEN
        var topicToSave = TaskTopicEntity.builder()
                .name("Varibales")
                .createdAt(now())
                .build();

        // WHEN
        var id = taskTopicRepository.save(topicToSave);
        var foundTopic = taskTopicRepository.getById(id).get();

        // THEN
        assertEquals(id, foundTopic.getId());
        assertEquals(topicToSave.getName(), foundTopic.getName());
        assertNotNull(foundTopic.getCreatedAt());
    }

    @Test
    void delete_happyPath() throws Exception {
        // GIVEN
        var firstId = createTopic("Variables");
        var secondId =createTopic("Collections");

        // WHEN
        taskTopicRepository.delete(firstId);
        Optional<TaskTopicEntity> firstTopic = taskTopicRepository.getById(firstId);
        var allTopics = taskTopicRepository.findAll(0, 5);

        // THEN
        assertFalse(firstTopic.isPresent());
        assertEquals(1, allTopics.size());
    }

    @Test
    void deleteAll_happyPath() throws Exception {
        // GIVEN
        var firstId = createTopic("Variables");
        var secondId = createTopic("Collections");

        // WHEN
        taskTopicRepository.deleteAll();
        var allTopics = taskTopicRepository.findAll(0, 5);

        // THEN
        assertTrue(allTopics.isEmpty());
    }

}