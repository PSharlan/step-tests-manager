package by.itstep.tasks;

import by.itstep.tasks.entity.TaskEntity;
import by.itstep.tasks.entity.TaskTopicEntity;
import by.itstep.tasks.repository.TaskRepository;
import by.itstep.tasks.repository.TaskTopicRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static java.time.LocalDateTime.now;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TasksApplicationTests {

	@Autowired
	protected TaskTopicRepository taskTopicRepository;

	@Autowired
	protected TaskRepository taskRepository;

	@BeforeEach
	public void before() {
		taskRepository.deleteAll();
		taskTopicRepository.deleteAll();
	}

	protected UUID createTopic(String topicName) {
		var topic = TaskTopicEntity.builder()
				.name(topicName)
				.createdAt(now())
				.build();

		return taskTopicRepository.save(topic);
	}

	protected UUID createTask(String exercise, UUID topicId) {
		var topic = taskTopicRepository.getById(topicId).get();

		var taskToSave = TaskEntity.builder().exercise(exercise)
				.exerciseDescription("Some description")
				.rightAnswer("Right answer")
				.topic(topic)
				.build();

		return taskRepository.save(taskToSave);
	}

}
