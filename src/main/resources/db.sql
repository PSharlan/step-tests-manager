drop table if exists task, task_topic;

create table task_topic
(
	id UUID
		constraint task_topic_pk
			primary key,
	name text not null,
	created_at timestamp not null,
	updated_at timestamp default null,
	deleted_at timestamp default null
);

create unique index task_topic_name_uindex
	on task_topic (name);

create table task
(
	id UUID
		constraint task_pk
			primary key,
	topic_id UUID not null,
	exercise text not null,
	exercise_description text not null,
	right_answer text not null,
	created_at timestamp not null,
	updated_at timestamp default null,
	deleted_at timestamp default null
);

alter table task
	add constraint task_task_topic_id_fk
		foreign key (topic_id) references task_topic;



