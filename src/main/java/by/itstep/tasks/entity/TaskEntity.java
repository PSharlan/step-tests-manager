package by.itstep.tasks.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, of = "id")
public class TaskEntity extends BaseEntity {

    private UUID id;
    private TaskTopicEntity topic;
    private String exercise;
    private String exerciseDescription;
    private String rightAnswer;

}
