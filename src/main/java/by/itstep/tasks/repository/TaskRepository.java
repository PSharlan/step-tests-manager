package by.itstep.tasks.repository;

import by.itstep.tasks.entity.TaskEntity;
import by.itstep.tasks.entity.TaskTopicEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TaskRepository {

    Optional<TaskEntity> getById(UUID id);

    List<TaskEntity> findAll(int page, int size);

    List<TaskEntity> findByTopic(TaskTopicEntity topic, int page, int size);

    UUID save(TaskEntity task);

    void delete(UUID id);

    void deleteAll();

}
