package by.itstep.tasks.repository.impl;

import by.itstep.tasks.entity.TaskEntity;
import by.itstep.tasks.entity.TaskTopicEntity;
import by.itstep.tasks.repository.TaskRepository;
import by.itstep.tasks.tables.records.TaskRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static by.itstep.tasks.Tables.TASK;
import static by.itstep.tasks.Tables.TASK_TOPIC;
import static by.itstep.tasks.mapper.TaskMapper.TASK_MAPPER;
import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.toList;

@Repository
@RequiredArgsConstructor
public class TaskRepositoryImpl implements TaskRepository {

    private final DSLContext dsl;

    @Override
    public Optional<TaskEntity> getById(UUID id) {
        Result<Record> result = dsl.select().
                from(TASK)
                .leftJoin(TASK_TOPIC).on(TASK.TOPIC_ID.eq(TASK_TOPIC.ID))
                .where(TASK.ID.eq(id).and(TASK.DELETED_AT.isNull()))
                .fetch();

        TaskEntity foundTask = null;
        if (!result.isEmpty()) {
            foundTask = TASK_MAPPER.toEntity(result.get(0));
        }
        return Optional.ofNullable(foundTask);
    }

    @Override
    public List<TaskEntity> findAll(int page, int size) {
        Result<Record> result = dsl.select()
                .from(TASK)
                .join(TASK_TOPIC).on(TASK.TOPIC_ID.eq(TASK_TOPIC.ID))
                .where(TASK.DELETED_AT.isNull())
                .orderBy(TASK.CREATED_AT.desc())
                .limit(page * size, size)
                .fetch();

        return result.stream()
                .map(TASK_MAPPER::toEntity)
                .collect(toList());
    }

    @Override
    public List<TaskEntity> findByTopic(TaskTopicEntity topic, int page, int size) {
        Result<Record> result = dsl.select()
                .from(TASK)
                .join(TASK_TOPIC).on(TASK.TOPIC_ID.eq(TASK_TOPIC.ID))
                .where(TASK.TOPIC_ID.eq(topic.getId()).and(TASK.DELETED_AT.isNull()))
                .orderBy(TASK.CREATED_AT.desc())
                .limit(page * size, size)
                .fetch();

        return result.stream()
                .map(TASK_MAPPER::toEntity)
                .collect(toList());
    }

    @Override
    public UUID save(TaskEntity task) {
        var id = randomUUID();
        dsl.insertInto(TASK)
                .set(TASK.ID, id)
                .set(TASK.EXERCISE, task.getExercise())
                .set(TASK.EXERCISE_DESCRIPTION, task.getExerciseDescription())
                .set(TASK.RIGHT_ANSWER, task.getRightAnswer())
                .set(TASK.CREATED_AT, now())
                .set(TASK.TOPIC_ID, task.getTopic().getId())
                .execute();

        return id;
    }

    @Override
    public void delete(UUID id) {
        dsl.update(TASK)
                .set(TASK.DELETED_AT, now())
                .where(TASK.ID.eq(id))
                .execute();
    }

    @Override
    public void deleteAll() {
        dsl.deleteFrom(TASK)
                .execute();
    }

}
