package by.itstep.tasks.repository.impl;

import by.itstep.tasks.entity.TaskTopicEntity;
import by.itstep.tasks.repository.TaskTopicRepository;
import by.itstep.tasks.tables.records.TaskTopicRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.exception.NoDataFoundException;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static by.itstep.tasks.mapper.TaskTopicMapper.TASK_TOPIC_MAPPER;
import static by.itstep.tasks.tables.TaskTopic.TASK_TOPIC;
import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.toList;

@Repository
@RequiredArgsConstructor
public class TaskTopicRepositoryImpl implements TaskTopicRepository {

    private final DSLContext dsl;

    @Override
    public Optional<TaskTopicEntity> getById(UUID id) {
        Result<TaskTopicRecord> result = dsl.selectFrom(TASK_TOPIC)
                .where(TASK_TOPIC.ID.eq(id).and(TASK_TOPIC.DELETED_AT.isNull()))
                .fetch();

        TaskTopicEntity foundTopic = null;
        if (!result.isEmpty()) {
            foundTopic = TASK_TOPIC_MAPPER.toEntity(result.get(0));
        }
        return Optional.ofNullable(foundTopic);
    }

    @Override
    public List<TaskTopicEntity> findAll(int page, int size) {
        Result<TaskTopicRecord> result = dsl.selectFrom(TASK_TOPIC)
                .where(TASK_TOPIC.DELETED_AT.isNull())
                .orderBy(TASK_TOPIC.CREATED_AT.desc())
                .limit(page * size, size)
                .fetch();

        return result.stream()
                .map(TASK_TOPIC_MAPPER::toEntity)
                .collect(toList());
    }

    @Override
    public UUID save(TaskTopicEntity task) {
        var id = randomUUID();
        dsl.insertInto(TASK_TOPIC)
                .set(TASK_TOPIC.ID, id)
                .set(TASK_TOPIC.NAME, task.getName())
                .set(TASK_TOPIC.CREATED_AT, task.getCreatedAt())
                .execute();

        return id;
    }

    @Override
    public void delete(UUID id) {
        dsl.update(TASK_TOPIC)
                .set(TASK_TOPIC.DELETED_AT, now())
                .where(TASK_TOPIC.ID.eq(id))
                .execute();
    }

    @Override
    public void deleteAll() {
        dsl.delete(TASK_TOPIC).execute();
    }

}
