package by.itstep.tasks.repository;

import by.itstep.tasks.entity.TaskTopicEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TaskTopicRepository {

    Optional<TaskTopicEntity> getById(UUID id);

    List<TaskTopicEntity> findAll(int page, int size);

    UUID save(TaskTopicEntity task);

    void delete(UUID id);

    void deleteAll();

}
