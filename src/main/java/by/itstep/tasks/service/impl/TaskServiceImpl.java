package by.itstep.tasks.service.impl;

import by.itstep.tasks.entity.TaskEntity;
import by.itstep.tasks.repository.TaskRepository;
import by.itstep.tasks.service.TaskService;
import by.itstep.tasks.service.TaskTopicService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final TaskTopicService taskTopicService;

    @Override
    public TaskEntity get(UUID id) {
        var foundTask = taskRepository.getById(id)
                .orElseThrow(() -> new RuntimeException("Task was not found!"));
        log.info("Task was fond: {} ", foundTask);
        return foundTask;
    }

    @Override
    public List<TaskEntity> findAll(int page, int size) {
        var foundTasks = taskRepository.findAll(page, size);
        log.info("Tasks were found. Page: {}, Size: {}, ", page, foundTasks.size());
        return foundTasks;
    }

    @Override
    public List<TaskEntity> findByTopic(UUID topicId, int page, int size) {
        var foundTopic = taskTopicService.get(topicId);
        var foundTasks = taskRepository.findByTopic(foundTopic, page, size);
        log.info("Tasks were found. Topic: {}, Page: {}, Size: {}, ", foundTopic.getName(), page, foundTasks.size());
        return foundTasks;
    }

    @Override
    public TaskEntity save(TaskEntity task) {
        var savedId = taskRepository.save(task);
        var savedTask = taskRepository.getById(savedId)
                .orElseThrow(() -> new RuntimeException("Saved but not found!"));
        log.info("Task was saved. Id: " + savedId);
        return savedTask;
    }

    @Override
    public void delete(UUID id) {
        taskRepository.delete(id);
        log.info("Task was deleted. Id: {}", id);
    }

}
