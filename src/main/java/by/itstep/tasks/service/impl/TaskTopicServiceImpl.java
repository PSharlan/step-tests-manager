package by.itstep.tasks.service.impl;

import by.itstep.tasks.entity.TaskTopicEntity;
import by.itstep.tasks.repository.TaskTopicRepository;
import by.itstep.tasks.service.TaskTopicService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskTopicServiceImpl implements TaskTopicService {

    private final TaskTopicRepository taskTopicRepository;

    @Override
    public TaskTopicEntity get(UUID id) {
        var foundTopic = taskTopicRepository.getById(id)
                .orElseThrow(() -> new RuntimeException("Topic was not found!"));
        log.info("Topic was fond: {} ", foundTopic);
        return foundTopic;
    }

    @Override
    public List<TaskTopicEntity> findAll(int page, int size) {
        var foundTopics = taskTopicRepository.findAll(page, size);
        log.info("Topics were found. Page: {}, Size: {}, ", page, foundTopics.size());
        return foundTopics;
    }

    @Override
    public TaskTopicEntity save(TaskTopicEntity task) {
        var savedId = taskTopicRepository.save(task);
        var savedTopic = taskTopicRepository.getById(savedId)
                .orElseThrow(() -> new RuntimeException("Saved but not found!"));
        log.info("Topic was saved. Id: " + savedId);
        return savedTopic;
    }

    @Override
    public void delete(UUID id) {
        taskTopicRepository.delete(id);
        log.info("Topic was deleted. Id: {}", id);
    }

}
