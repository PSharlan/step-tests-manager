package by.itstep.tasks.service;

import by.itstep.tasks.entity.TaskTopicEntity;

import java.util.List;
import java.util.UUID;

public interface TaskTopicService {

    TaskTopicEntity get(UUID id);

    List<TaskTopicEntity> findAll(int page, int size);

    TaskTopicEntity save(TaskTopicEntity task);

    void delete(UUID id);

}
