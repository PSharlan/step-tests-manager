package by.itstep.tasks.service;

import by.itstep.tasks.entity.TaskEntity;

import java.util.List;
import java.util.UUID;

public interface TaskService {

    TaskEntity get(UUID id);

    List<TaskEntity> findAll(int page, int size);

    List<TaskEntity> findByTopic(UUID topicId, int page, int size);

    TaskEntity save(TaskEntity task);

    void delete(UUID id);

}
