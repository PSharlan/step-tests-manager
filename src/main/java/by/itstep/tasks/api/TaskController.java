package by.itstep.tasks.api;

import by.itstep.tasks.entity.TaskEntity;
import by.itstep.tasks.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/tasks")
public class TaskController {

    private final TaskService taskService;

    @GetMapping("/{id}")
    public ResponseEntity<TaskEntity> get(@PathVariable UUID id) {
        var foundTask = taskService.get(id);
        return new ResponseEntity<>(foundTask, OK);
    }

    @GetMapping
    public ResponseEntity<List<TaskEntity>> findAll(@RequestParam int page, @RequestParam int size) {
        var foundTasks = taskService.findAll(page, size);
        return new ResponseEntity<>(foundTasks, OK);
    }

    @GetMapping("/filter")
    public ResponseEntity<List<TaskEntity>> findByTopic(@RequestParam int page, @RequestParam int size,
                                                @RequestParam UUID topicId) {
        var foundTasks = taskService.findByTopic(topicId, page, size);
        return new ResponseEntity<>(foundTasks, OK);
    }

    @PostMapping
    public ResponseEntity<TaskEntity> create(@RequestBody TaskEntity task) {
        var savedTask = taskService.save(task);
        return new ResponseEntity<>(savedTask, CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        taskService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
