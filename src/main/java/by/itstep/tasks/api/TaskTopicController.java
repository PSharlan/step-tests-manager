package by.itstep.tasks.api;

import by.itstep.tasks.entity.TaskTopicEntity;
import by.itstep.tasks.service.TaskTopicService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/tasks/topics")
public class TaskTopicController {

    private final TaskTopicService taskTopicService;

    @GetMapping("/{id}")
    public ResponseEntity<TaskTopicEntity> get(@PathVariable UUID id) {
        var foundTopic = taskTopicService.get(id);
        return new ResponseEntity<>(foundTopic, OK);
    }

    @GetMapping
    public ResponseEntity<List<TaskTopicEntity>> findAll(@RequestParam int page, @RequestParam int size) {
        var foundTopics = taskTopicService.findAll(page, size);
        return new ResponseEntity<>(foundTopics, OK);
    }

    @PostMapping
    public ResponseEntity<TaskTopicEntity> create(@RequestBody TaskTopicEntity task) {
        var savedTopic = taskTopicService.save(task);
        return new ResponseEntity<>(savedTopic, CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        taskTopicService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }
    
}
