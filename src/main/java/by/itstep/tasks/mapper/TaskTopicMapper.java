package by.itstep.tasks.mapper;

import by.itstep.tasks.Tables;
import by.itstep.tasks.entity.TaskTopicEntity;
import by.itstep.tasks.tables.records.TaskTopicRecord;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(imports = Tables.class)
public interface TaskTopicMapper {

    TaskTopicMapper TASK_TOPIC_MAPPER = Mappers.getMapper(TaskTopicMapper.class);

    @Mapping(target = "id", expression = "java(record.getValue(Tables.TASK_TOPIC.ID))")
    @Mapping(target = "name", expression = "java(record.getValue(Tables.TASK_TOPIC.NAME))")
    @Mapping(target = "createdAt", expression = "java(record.getValue(Tables.TASK_TOPIC.CREATED_AT))")
    @Mapping(target = "updatedAt", expression = "java(record.getValue(Tables.TASK_TOPIC.UPDATED_AT))")
    @Mapping(target = "deletedAt", expression = "java(record.getValue(Tables.TASK_TOPIC.DELETED_AT))")
    TaskTopicEntity toEntity(TaskTopicRecord record);

}
