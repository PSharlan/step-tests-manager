package by.itstep.tasks.mapper;

import by.itstep.tasks.Tables;
import by.itstep.tasks.entity.TaskEntity;
import org.jooq.Record;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(imports = Tables.class)
public interface TaskMapper {

    TaskMapper TASK_MAPPER = Mappers.getMapper(TaskMapper.class);

    @Mapping(target = "id", expression = "java(record.getValue(Tables.TASK.ID))")
    @Mapping(target = "exercise", expression = "java(record.getValue(Tables.TASK.EXERCISE))")
    @Mapping(target = "exerciseDescription", expression = "java(record.getValue(Tables.TASK.EXERCISE_DESCRIPTION))")
    @Mapping(target = "rightAnswer", expression = "java(record.getValue(Tables.TASK.RIGHT_ANSWER))")
    @Mapping(target = "createdAt", expression = "java(record.getValue(Tables.TASK.CREATED_AT))")
    @Mapping(target = "updatedAt", expression = "java(record.getValue(Tables.TASK.UPDATED_AT))")
    @Mapping(target = "deletedAt", expression = "java(record.getValue(Tables.TASK.DELETED_AT))")
    @Mapping(target = "topic.id", expression = "java(record.getValue(Tables.TASK_TOPIC.ID))")
    @Mapping(target = "topic.name", expression = "java(record.getValue(Tables.TASK_TOPIC.NAME))")
    @Mapping(target = "topic.createdAt", expression = "java(record.getValue(Tables.TASK_TOPIC.CREATED_AT))")
    @Mapping(target = "topic.updatedAt", expression = "java(record.getValue(Tables.TASK_TOPIC.UPDATED_AT))")
    @Mapping(target = "topic.deletedAt", expression = "java(record.getValue(Tables.TASK_TOPIC.DELETED_AT))")
    TaskEntity toEntity(Record record);

}
